# Gemnasium Maven Plugin changelog

## 0.4.0 / 2020-03-11

* updates all dependencies to their latest versions
* removed features, code and documentation about Gemnasium projects, hosted on Gemnasium.com 
* added publishing job, to automatically publish releases to maven central
* add the google java styleguide as mandatory checkstyle rules

## 0.3.0 / 2018-01-29

* Fix: don't fail the build when `create-project` goal can't create the gemnasium.properties file
* Feature: add a `dump-dependencies` goal that write project's dependencies to a json file

## 0.2.0 / 2017-11-02

* Fix upload payload (wrong parameter for filename)
* Improve generated dependency file content (send more data)

## 0.1.0 / 2017-11-02

Initial release
