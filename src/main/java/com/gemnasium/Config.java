package com.gemnasium;

import java.io.File;
import org.apache.maven.plugin.MojoExecutionException;

/**
 * Gemnasium Maven Plugin configuration.
 */
public class Config {

  private File baseDir;

  private String ignoredScopes;

  /**
   * Initializes a the plugin configuration with the following ascending priority: - plugin
   * configuration (within pom.xml) - env variables
   * 
   * @param baseDir The maven project baseDir.
   * @param ignoredScopes Comma separated list of Maven dependency scopes to ignore.
   * @throws MojoExecutionException if properties configuration can't be loaded.
   */
  public Config(File baseDir, String ignoredScopes) throws MojoExecutionException {
    this.baseDir = baseDir;

    this.ignoredScopes =
        getFirstNotEmpty(System.getenv().get("GEMNASIUM_IGNORED_SCOPES"), ignoredScopes);
  }

  private String getFirstNotEmpty(String envVarConfig, String pluginConfig) {
    if (envVarConfig != null && !envVarConfig.isEmpty()) {
      return envVarConfig;
    }

    if (pluginConfig != null && !pluginConfig.isEmpty()) {
      return pluginConfig;
    }

    return pluginConfig;
  }

  public String toString() {
    return "baseDir:" + baseDir + "\n" + "ignoredScopes:" + ignoredScopes + "\n";
  }

  // Getters and setters
  public File getBaseDir() {
    return baseDir;
  }

  public String getIgnoredScopes() {
    return ignoredScopes;
  }

  public void setIgnoredScopes(String ignoredScopes) {
    this.ignoredScopes = ignoredScopes;
  }

}
