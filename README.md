# Gemnasium Maven Plugin

The Gemnasium maven plugin helps you manage your project dependencies with Gemnasium. Gemnasium keeps track of project dependencies and send notifications when new versions are released or security advisories are published.

## How to install it?

The Gemnasium Maven Plugin is published on the Maven Central Repository so you just need to add the plugin to your project's `pom.xml`:

```xml
<build>
  <plugins>
    <plugin>
      <groupId>com.gemnasium</groupId>
      <artifactId>gemnasium-maven-plugin</artifactId>
      <version>0.4.0</version>
    </plugin>
  </plugins>
</build>
```

### Dump your project's dependencies to a json file

You can write the list your project dependencies to a json file without any setup:

    mvn gemnasium:dump-dependencies

You can ignore some dependencies with the `ignoredScope` option (see [Configuration](#configuration) section below).

    mvn gemnasium:dump-dependencies -DignoredScopes=test

## Configuration

There are 2 ways to configure the Gemnasium Maven Plugin, in ascending priority order:

### Plugin configuration within pom.xml

The configuration can also be provided directly within `pom.xml`.

```xml
<build>
  <plugins>
    <plugin>
      <groupId>com.gemnasium</groupId>
      <artifactId>gemnasium-maven-plugin</artifactId>
      <version>0.4.0</version>
      <configuration>
        <ignoredScopes>test</ignoredScopes>
      </configuration>
    </plugin>
  </plugins>
</build>
```
NB: Options set in `pom.xml` are overriden by env vars.

### Environment variables

The configuration can finally be provided via environment variables. Env vars take precedence over all other configuration sources.

    GEMNASIUM_IGNORED_SCOPES=test gemnasium:dump-dependencies

See options details below to kow about available env vars.

### Options details

Key name | Env Var name | Description
---------- | ------- | -----------
ignoredScopes | GEMNASIUM_IGNORED_SCOPES | **(Optional)** Comma separated list of Maven dependency scopes to ignore. ([See Maven documentation](https://maven.apache.org/guides/introduction/introduction-to-dependency-mechanism.html#Dependency_Scope)).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.
